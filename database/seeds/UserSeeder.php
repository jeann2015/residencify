<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'Jean Carlos Nunez',
            'email'     => 'jeancarlosn2008@gmail.com',
            'role'   => 'Admin',
            'first_time' => 1,
            'chief_users_id' => 1,
            'place_id' => 1,
            'password'  => bcrypt('123Jeann123'),
            'remember_token' => str_random(10),
            'apartment' => '-',
            'description' => '-',
        ]);

        User::create([
            'name'      => 'Idekel',
            'email'     => 'i@example.com',
            'role'   => 'Chief',
            'first_time' => 1,
            'chief_users_id' => 2,
            'place_id' => 1,
            'password'  => bcrypt('123123123'),
            'remember_token' => str_random(10),
            'apartment' => '1-A',
            'description' => '-',
        ]);

        User::create([
            'name'      => 'Francisco Solis',
            'email'     => 'fs@example.com',
            'role'   => 'Owner',
            'first_time' => 1,
            'chief_users_id' => 2,
            'place_id' => 1,
            'password'  => bcrypt('123123123'),
            'remember_token' => str_random(10),
            'apartment' => '1-A',
            'description' => '-',
        ]);

        User::create([
            'name'      => 'Sergio',
            'email'     => 's@example.com',
            'role'   => 'Owner',
            'first_time' => 1,
            'chief_users_id' => 2,
            'place_id' => 1,
            'password'  => bcrypt('123123123'),
            'remember_token' => str_random(10),
            'apartment' => '1-A',
            'description' => '-',
        ]);

        User::create([
            'name'      => 'Edgar',
            'email'     => 'e@example.com',
            'role'   => 'Owner',
            'first_time' => 1,
            'chief_users_id' => 2,
            'place_id' => 1,
            'password'  => bcrypt('123123123'),
            'remember_token' => str_random(10),
            'apartment' => '1-A',
            'description' => '-',
        ]);



    }
}

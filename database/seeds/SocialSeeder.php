<?php

use App\Social;
use Illuminate\Database\Seeder;

class SocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Social::create([
            'description'      => 'Area Social con Piscina',
        ]);

        Social::create([
            'description'      => 'Area Social sin Piscina',
        ]);
    }
}

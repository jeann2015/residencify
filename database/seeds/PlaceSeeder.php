<?php

use App\Place;
use Illuminate\Database\Seeder;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Place::create([
           'description' => 'BuildExample1',
           'address' => 'Panama',
           'phone' => '123-1234-12345',
           'chief_users_id' => 2
        ]);

        Place::create([
            'description' => 'BuildExample2',
            'address' => 'Panama',
            'phone' => '123-1234-12345',
            'chief_users_id' => 2
        ]);
    }
}

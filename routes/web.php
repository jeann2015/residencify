<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{locale}', 'LocalizationController@index')->name('lang');

Route::get('/', function () {
    return redirect('login');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

//Routes Users
Route::get('/users', 'UserController@index')->name('users');

Route::get('/users/create', 'UserController@create')->name('users.create');
Route::post('/users/store', 'UserController@store')->name('users.store');

Route::get('/users/edit/{id}', 'UserController@edit')->name('users.edit');
Route::post('/users/update', 'UserController@update')->name('users.update');

Route::get('/users/delete/{id}', 'UserController@delete')->name('users.delete');
Route::post('/users/destroy', 'UserController@destroy')->name('users.destroy');
//Routes Users

//Routes Places
Route::get('/places', 'PlaceController@index')->name('places');

Route::get('/places/create', 'PlaceController@create')->name('places.create');
Route::post('/places/store', 'PlaceController@store')->name('places.store');

Route::get('/places/edit/{id}', 'PlaceController@edit')->name('places.edit');
Route::post('/places/update', 'PlaceController@update')->name('places.update');

Route::get('/places/delete/{id}', 'PlaceController@delete')->name('places.delete');
Route::post('/places/destroy', 'PlaceController@destroy')->name('places.destroy');
//Routes Places

//Routes Reservations
Route::get('/reservations', 'ReservationController@index')->name('reservations');

Route::get('/reservations/create', 'ReservationController@create')->name('reservations.create');
Route::post('/reservations/store', 'ReservationController@store')->name('reservations.store');

Route::get('/reservations/edit/{id}', 'ReservationController@edit')->name('reservations.edit');
Route::post('/reservations/update', 'ReservationController@update')->name('reservations.update');

Route::get('/reservations/view/{id}', 'ReservationController@view')->name('reservations.view');

//Route::get('/reservations/delete/{id}', 'ReservationController@delete')->name('reservations.delete');
//Route::post('/reservations/destroy', 'ReservationController@destroy')->name('reservations.destroy');
//Routes Reservations

//Routes Claim
Route::get('/claims', 'ClaimController@index')->name('claims');


Route::get('/claims/create', 'ClaimController@create')->name('claims.create');
Route::post('/claims/store', 'ClaimController@store')->name('claims.store');

Route::get('/claims/edit/{id}', 'ClaimController@edit')->name('claims.edit');
Route::post('/claims/update', 'ClaimController@update')->name('claims.update');

//Route::get('/claims/delete/{id}', 'ClaimController@delete')->name('claims.delete');
//Route::post('/claims/destroy', 'ClaimController@destroy')->name('claims.destroy');
//Routes Claim

//Routes users
Route::get('admin/password', 'UserController@password')->name('admin.password');

Route::post('change', 'UserController@change')->name('change.password');
//Routes users

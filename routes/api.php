<?php

use App\Message;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/messages', function (Request $request) {
    $place = User::find($request->user_id);

    $m = Message::create([
        'message' => $request->message,
        'user_id' => $request->user_id,
        'place_id' => $place->place_id
    ]);

    return response()->json($m->count());
});

Route::get('/AllMessages', function (Request $request) {
    $messages = Message::where('created_at','>=' , Carbon::now()->subDays(29))->get();
    return response()->json($messages->count());
});


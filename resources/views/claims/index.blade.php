@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Claims') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            <!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="col-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
            <div class="box-body">
                <a href="{{ route('claims.create') }}" class="btn btn-success">
                    <i class="far fa-file-alt">
                    </i>
                    {{ __('messages.New claim') }}
                </a>

                <table class="table table-bordered ">
                    <tr>
                        <th>{{ __('messages.Users') }}</th>
                        <th>{{ __('messages.Status') }}</th>
                        <th>{{ __('messages.Actions') }}</th>
                    </tr>
                    @foreach($claims as $claim)
                        <tr>

                            <td>
                                @if(auth()->user()->role == 'Chief')
                                    @if($claim->kind == 'Anonymous')
                                        {{ __('messages.'.$claim->kind) }}
                                    @else
                                        {{$claim->user->name}}
                                    @endif
                                @else
                                    {{$claim->user->name}}
                                @endif
                            </td>

                            <td>
                                @if($claim->status == 'Wait')
                                    <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 New Messages">{{ __('messages.Wait') }}</span>
                                @endif
                                @if($claim->status == 'Ready')
                                        <span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="3 New Messages">{{ __('messages.Ready') }}</span>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group-vertical">
                                    <a href="{{ route('claims.edit',$claim->id) }}" class="btn btn-success">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@stop

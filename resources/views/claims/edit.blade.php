@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Claims') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            @if ($errors->any())
                <div class="alert alert-success">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <section class="content">
                <div class="row">
                <div class="col-md-12">
                <div class="box">
                <div class="box-body">
                <a href="{{ route('claims') }}"  class="btn btn-success">
                    <i class="fa fa-arrow-circle-left">
                    </i>
                    {{ __('messages.Back') }}
                </a>
                <form action="{{route('claims.update')}}" method="post">
                    @csrf
                    <table class="table table-bordered ">
                    <tr>
                        <th>{{ __('messages.Description') }}</th>
                        <th>
                            <textarea class="form-control" cols="10" rows="10" id="details" name="details" >
                                {{ $claims->details }}
                            </textarea>
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Kind') }}</th>
                        <th>
                            {{ Form::select('kind',['Anonymous' => 'Anonymous', 'No Anonymous' => 'No Anonymous'],$claims->kind, ['class'=>'form-control','require'=>'true','id'=>'kind']) }}
                        </th>
                    </tr>
                    @if (auth()->user()->role == 'Chief' || auth()->user()->role == 'Admin')
                        <tr>
                            <th>{{ __('messages.Status') }}</th>
                            <th>
                                {{ Form::select('status',['Wait' => __('messages.Wait'), 'Ready' => __('messages.Ready')], $claims->status , ['class'=>'form-control','require'=>'true','id'=>'status']) }}
                            </th>
                        </tr>
                    @endif
                </table>
                <button type="submit" class="btn btn-app">
                    <input id="id" name="id" type="hidden" value="{{$claims->id}}">
                    <i class="fa fa-save"></i> {{ __('messages.Update') }}
                </button>
                </form>
            </div>
        </div>
        </div>
@stop

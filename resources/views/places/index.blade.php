@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Places') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            <!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="col-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
            <div class="box-body">
                <a href="{{ route('places.create') }}" class="btn btn-success">
                    <i class="far fa-file-alt">
                    </i>
                    {{ __('messages.New place') }}
                </a>

                <table class="table table-bordered ">
                    <tr>
                        <th>{{ __('messages.Description') }}</th>
                        <th>{{ __('messages.Phone') }}</th>
                        <th>{{ __('messages.Actions') }}</th>
                    </tr>
                    @foreach($places as $place)
                        <tr>
                            <td>{{$place->description}}</td>
                            <td>{{$place->phone}}</td>
                            <td>
                                <div class="btn-group-vertical">
                                    <a href="{{ route('places.edit',$place->id) }}" class="btn btn-success">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ route('places.delete',$place->id) }}" class="btn btn-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@stop

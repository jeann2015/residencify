@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Places') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            @if ($errors->any())
                <div class="alert alert-success">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <section class="content">
                <div class="row">
                <div class="col-md-12">
                <div class="box">
                <div class="box-body">
                <a href="{{ route('places') }}"  class="btn btn-success">
                    <i class="fa fa-arrow-circle-left">
                    </i>
                    {{ __('messages.Back') }}
                </a>
                <form action="{{route('places.update')}}" method="post">
                    @csrf
                    <table class="table table-bordered ">
                    <tr>
                        <th>{{ __('messages.Description') }}</th>
                        <th>
                            <input class="form-control" type="text" id="description" name="description" value="{{ $places->description }}">
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Address') }}</th>
                        <th>
                            <input class="form-control" type="text" id="address" name="address" value="{{ $places->address }}">
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Phone') }}</th>
                        <th>
                            <input class="form-control" type="text" id="phone" name="phone" value="{{ $places->phone }}">
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Chief') }}</th>
                        <th>
                            <select class="form-control" name="chief_users_id" id="chief_users_id">
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </th>
                    </tr>
                </table>
                <button type="submit" class="btn btn-app">
                    <input id="id" name="id" type="hidden" value="{{ $places->id }}">
                    <i class="fa fa-save"></i> {{ __('messages.Edit') }}
                </button>
                </form>
            </div>
        </div>
        </div>
@stop

@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Reservations') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            @if ($errors->any())
                <div class="alert alert-success">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <section class="content">
                <div class="row">
                <div class="col-md-12">
                <div class="box">
                <div class="box-body">
                <a href="{{ route('reservations') }}"  class="btn btn-success">
                    <i class="fa fa-arrow-circle-left">
                    </i>
                    {{ __('messages.Back') }}
                </a>
                <form action="{{route('reservations.destroy')}}" method="post">
                    @csrf
                    <table class="table table-bordered ">
                        <tr>
                            <th>{{ __('messages.Date') }}</th>
                            <th>
                                <input class="form-control" type="date" id="dateReservation" name="dateReservation" value="{{ \Carbon\Carbon::createFromDate($reservations->start)->toDateString('MM-DD-YYYY') }}">
                            </th>
                        </tr>
                    <tr>
                        <th>{{ __('messages.Start') }}</th>
                        <th>
                            {{ Form::select('start',['13:00:00' => '01:00:00 PM', '14:00:00' => '02:00:00 PM'],\Carbon\Carbon::createFromDate($reservations->start)->toTimeString('h:mm:ss A') , ['class'=>'form-control','require'=>'true','id'=>'start']) }}
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.End') }}</th>
                        <th>
                            {{ Form::select('end',['17:00:00' => '05:00:00 PM', '18:00:00' => '06:00:00 PM'],\Carbon\Carbon::createFromDate($reservations->end)->toTimeString('h:mm:ss A'), ['class'=>'form-control','require'=>'true','id'=>'end']) }}
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Social') }}</th>
                        <th>
                            {{ Form::select('social_id',Arr::pluck($socials, 'description', 'id'),$reservations->social_id, ['class'=>'form-control','require'=>'true','id'=>'social_id']) }}
                        </th>
                    </tr>
                </table>
                <button type="submit" class="btn btn-app">
                    <input type="hidden" id="id" name="id" value="{{$reservations->id}}">
                    <i class="fa fa-save"></i> {{ __('messages.Delete') }}
                </button>
                </form>
            </div>
        </div>
        </div>
@stop

@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Reservations') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            <!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="col-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
            <div class="box-body">
                <a href="{{ route('reservations.create') }}" class="btn btn-success">
                    <i class="far fa-file-alt">
                    </i>
                    {{ __('messages.New reservation') }}
                </a>

                <table class="table table-bordered ">
                    <tr>
                        <th>{{ __('messages.Users') }}</th>
                        <th>{{ __('messages.Status') }}</th>
                        <th>{{ __('messages.Actions') }}</th>
                    </tr>
                    @foreach($reservations as $reservation)
                        <tr>
                            <td>
                                {{
                                    $reservation->user->name ." (" . $reservation->user->apartment . ") " . __('messages.'.date_format($reservation->created_at,"l")) ." ". date_format($reservation->created_at,"d") ." ".__('messages.of')." ".__('messages.'.date_format($reservation->created_at,"F") ) ." ". date_format($reservation->created_at,"Y h:i")
                                }}
                            </td>
                            <td>
                                @if($reservation->status == 'Wait')
                                    <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 New Messages">{{ __('messages.Wait') }}</span>
                                @endif
                                @if($reservation->status == 'Ready')
                                        <span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="3 New Messages">{{ __('messages.Ready') }}</span>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group-vertical">
                                    @if (@auth()->user()->role == 'Admin' || @auth()->user()->role == 'Chief')
                                        <a href="{{ route('reservations.edit',$reservation->id) }}" class="btn btn-success">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    @endif

                                    <a href="{{ route('reservations.view',$reservation->id) }}" class="btn btn-primary">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@stop

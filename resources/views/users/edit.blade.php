@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Users') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            @if ($errors->any())
                <div class="alert alert-success">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <section class="content">
                <div class="row">
                <div class="col-md-12">
                <div class="box">
                <div class="box-body">
                <a href="{{ route('users') }}"  class="btn btn-success">
                    <i class="fa fa-arrow-circle-left">
                    </i>
                    {{ __('messages.Back') }}
                </a>
                <form action="{{route('users.update')}}" method="post">
                    @csrf
                    <table class="table table-bordered ">
                    <tr>
                        <th>{{ __('messages.Name') }}</th>
                        <th>
                            <input class="form-control" type="text" id="name" name="name" value="{{ $users->name }}">
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Email') }}</th>
                        <th>
                            <input class="form-control" type="email" id="email" name="email" value="{{ $users->email }}">
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Role') }}</th>
                        <th>
                            <select required class="form-control" name="role" id="role">
                                @if(@auth()->user()->role == 'Admin')
                                    <option @if( $users->role == 'Admin' ) {{ 'selected' }} @endif value="Admin">Admin</option>
                                    <option @if( $users->role == 'Chief' ) {{ 'selected' }} @endif value="Chief">Chief</option>
                                    <option @if( $users->role == 'Owner' ) {{ 'selected' }} @endif value="Owner">Owner</option>
                                    <option @if( $users->role == 'Tenant' ) {{ 'selected' }} @endif value="Tenant">Tenant</option>
                                @elseif(@auth()->user()->role == 'Chief')
                                    <option @if( $users->role == 'Owner' ) {{ 'selected' }} @endif value="Owner">Owner</option>
                                    <option @if( $users->role == 'Tenant' ) {{ 'selected' }} @endif value="Tenant">Tenant</option>
                                @elseif(@auth()->user()->role == 'Owner')
                                    <option @if( $users->role == 'Tenant' ) {{ 'selected' }} @endif value="Tenant">Tenant</option>
                                @endif
                            </select>
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Place') }}</th>
                        <th>
                            {{ Form::select('place_id',Arr::pluck($places, 'description', 'id'),$users->place_id, ['class'=>'form-control','require'=>'true','id'=>'place_id']) }}
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Apartment') }}</th>
                        <th>
                            <input class="form-control" type="text" id="apartment" name="apartment" value="{{ $users->apartment }}">
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Other Information') }}</th>
                        <th>
                            <textarea rows="10" cols="5" class="form-control"  id="description" name="description" > {{ $users->description }}</textarea>
                        </th>
                    </tr>
                </table>
                <input type="hidden" value="{{ $users->id }}" name="id">
                <button type="submit" class="btn btn-app">
                    <i class="fa fa-save"></i> {{ __('messages.Update') }}
                </button>
                </form>
            </div>
        </div>
        </div>
@stop

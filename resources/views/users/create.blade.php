@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Users') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            @if ($errors->any())
                <div class="alert alert-success">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <section class="content">
                <div class="row">
                <div class="col-md-12">
                <div class="box">
                <div class="box-body">
                <a href="{{ route('users') }}"  class="btn btn-success">
                    <i class="fa fa-arrow-circle-left">
                    </i>
                    {{ __('messages.Back') }}
                </a>
                <form action="{{route('users.store')}}" method="post">
                    @csrf
                    <table class="table table-bordered ">
                    <tr>
                        <th>{{ __('messages.Name') }}</th>
                        <th>
                            <input class="form-control" type="text" id="name" name="name" value="">
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Email') }}</th>
                        <th>
                            <input class="form-control" type="email" id="email" name="email" value="">
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Role') }}</th>
                        <th>
                            <select required class="form-control" name="role" id="role">
                                @if(@auth()->user()->role == 'Admin')
                                    <option value="Admin">Admin</option>
                                    <option value="Chief">Chief</option>
                                    <option value="Owner">Owner</option>
                                    <option value="Tenant">Tenant</option>
                                    <option value="Concierge">Concierge</option>
                                @elseif(@auth()->user()->role == 'Chief')
                                    <option value="Owner">Owner</option>
                                    <option value="Tenant">Tenant</option>
                                    <option value="Concierge">Concierge</option>
                                @elseif(@auth()->user()->role == 'Owner')
                                    <option value="Tenant">Tenant</option>
                                @endif
                            </select>
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Place') }}</th>
                        <th>
                            {{ Form::select('place_id',Arr::pluck($places, 'description', 'id'),'', ['class'=>'form-control','require'=>'true','id'=>'place_id']) }}
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Apartment') }}</th>
                        <th>
                            <input class="form-control" type="text" id="apartment" name="apartment" value="">
                        </th>
                    </tr>
                    <tr>
                        <th>{{ __('messages.Other Information') }}</th>
                        <th>
                            <textarea rows="10" cols="5" class="form-control"  id="description" name="description" value=""> </textarea>
                        </th>
                    </tr>
                </table>
                <button type="submit" class="btn btn-app">
                    <i class="fa fa-save"></i> {{ __('messages.Save') }}
                </button>
                </form>
            </div>
        </div>
        </div>
@stop

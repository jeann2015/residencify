@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Change password') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            <div class="col-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
            <section class="content">
                <div class="row">
                <div class="col-md-12">
                <div class="box">
                <div class="box-body">
                <form action="{{route('change.password')}}" method="post">
                    @csrf
                    <table class="table table-bordered ">
                    <tr>
                        <th>{{ __('messages.Password') }}</th>
                        <th>
                            <input class="form-control" type="password" id="password" name="password" value="" required>
                        </th>
                    </tr>
                </table>
                <input type="hidden" value="{{ Auth::id() }}" name="id">
                <button type="submit" class="btn btn-app">
                    <i class="fa fa-save"></i> {{ __('messages.Change') }}
                </button>
                </form>
            </div>
        </div>
        </div>
@stop

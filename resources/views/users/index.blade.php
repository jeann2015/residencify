@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Users') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            <!-- Main content -->
            <section class="content">
                <div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <a href="{{ route('users.create') }}" class="btn btn-success">
                    <i class="far fa-file-alt">
                    </i>
                    {{ __('messages.New user') }}
                </a>
                <table class="table table-bordered ">
                    <tr>
                        <th>{{ __('messages.Email') }}</th>
                        <th>{{ __('messages.Role') }}</th>
                        <th>{{ __('messages.Actions') }}</th>
                    </tr>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role}}</td>
                            <td>
                                <div class="btn-group-vertical">
                                    <a href="{{ route('users.edit',$user->id) }}" class="btn btn-success">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href="{{ route('users.delete',$user->id) }}" class="btn btn-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@stop

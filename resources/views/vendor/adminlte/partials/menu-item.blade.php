@if (is_string($item))
    <li class="header">{{  Auth()->user()->name }}</li>
    <li class="header">{{  __('messages.'.$item) }}</li>
@elseif (isset($item['header']))
    <li class="header">{{ __('messages.'.$item['header']) }}</li>
@elseif (isset($item['search']) && $item['search'])
    <form action="{{ $item['href'] }}" method="{{ $item['method'] }}" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="{{ $item['input_name'] }}" class="form-control" placeholder="
            {{ __('messages.'.$item['text']) }}
          ">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fas fa-search"></i>
                </button>
              </span>
        </div>
      </form>
@else
<li class="{{ $item['class'] }}">
        @if (isset($item['auth']) && Arr::where($item['auth'], function ($value, $index){
                        return $value ===   @auth()->user()->role ? true : false;
        }))
            <a href="{{ $item['href'] }}"
               @if (isset($item['target'])) target="{{ $item['target'] }}" @endif
            >
            <i class="{{ $item['icon'] ?? 'far fa-fw fa-circle' }} {{ isset($item['icon_color']) ? 'text-' . $item['icon_color'] : '' }}"></i>
            <span>

                {{ __('messages.'.$item['text']) }}
            </span>
        @endif

        @if (isset($item['label']))
            <span class="pull-right-container">
                <span class="label label-{{ $item['label_color'] ?? 'primary' }} pull-right">{{ __('messages.'.$item['label']) }}</span>
            </span>
        @elseif (isset($item['submenu']))
            <span class="pull-right-container">
                <i class="fas fa-angle-left pull-right"></i>
            </span>
        @endif
    </a>
    @if (isset($item['submenu']))
        <ul class="{{ $item['submenu_class'] }}">
            @each('adminlte::partials.menu-item', $item['submenu'], 'item')
        </ul>
    @endif
</li>
@endif

@extends('adminlte::page')

@section('title', config('app.name', 'Laravel') )

@section('content_header')
    <h1>{{ __('messages.Dashboard') }}</h1>
@stop

@section('content')
    <div class="box box-danger direct-chat direct-chat-danger">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('messages.Direct Chat') }}</h3>
            <div class="box-tools pull-right">
{{--                <span data-toggle="tooltip" title="3 New Messages" class="badge bg-red">3</span>--}}
{{--                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>--}}
                <!-- In box-tools add this button if you intend to use the contacts pane -->
{{--                <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>--}}
{{--                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div id="messages" class="direct-chat-messages">
                @foreach($messages as $message)
                    @if($message->user_id === @auth()->user()->id )
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left">{{ $message->user->name }}</span>
                                <span class="direct-chat-timestamp pull-right">{{ __('messages.'.date_format($message->created_at,"l")) ." ". date_format($message->created_at,"d") ." ".__('messages.of')." ".__('messages.'.date_format($message->created_at,"F") ) ." ".date_format($message->created_at,"Y h:i A")  }}</span>
                            </div>
                            <div class="direct-chat-text">
                                {{ $message->message }}
                            </div>
                        </div>
                    @else
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right">{{ $message->user->name }}</span>
                                <span class="direct-chat-timestamp pull-left">{{ __('messages.'.date_format($message->created_at,"l")) ." ". date_format($message->created_at,"d") ." ".__('messages.of')." ".__('messages.'.date_format($message->created_at,"F") ) ." ".date_format($message->created_at,"Y h:i A")  }}</span>
                            </div>
                            <div class="direct-chat-text">
                                {{ $message->message }}
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
        <div class="box-footer">
            <div class="input-group">
               <input type="text" id="message" name="message" placeholder="{{ __('messages.Type Message ...') }}" class="form-control">
                <span class="input-group-btn">
                    <input class="form-control" type="hidden" onkeypress="sendMessageKeypress(e)" id="user_id" name="user_id" value="{{ Auth()->user()->id }}">
                    <input type="hidden" id="countMessages" name="countMessages" value="{{ $messages->count() }}">
                <button onclick="sendMessage()"  type="button" class="btn btn-danger btn-flat">{{ __('messages.Send') }}</button>
            </span>
            </div>
        </div>
    </div>
    <script>
        const messages = document.getElementById('messages');

        function scrollToBottom() {
            messages.scrollTop = messages.scrollHeight;
        }

        function sendMessage() {
            const  message = $('#message').val();
            const  user_id = $('#user_id').val();
            $.get( "api/messages/", { message: message, user_id: user_id })
                .done(function( data ) {
                    $('#message').val('');
                    location.reload();
                });
            scrollToBottom();
        }

        function sendMessageKeypress(e) {
            if (e.keyCode === 13) {
                const message = $('#message').val();
                const user_id = $('#user_id').val();
                $.get("api/messages/", {message: message, user_id: user_id})
                    .done(function (data) {
                        $('#message').val('');
                        location.reload();
                    });
                scrollToBottom();
            }
        }

        scrollToBottom();

        function getMessage() {
            const  countMessages = $('#countMessages').val();
            $.get( "api/AllMessages/")
                .done(function( data ) {
                    if(data > countMessages) {
                        location.reload();
                    }
                });
        }
        setInterval(getMessage, 3000);
    </script>
@stop

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines English
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /***Users Module***/
    'Dashboard' => 'Tablero',
    'Direct Chat' => 'Chat Directo',
    'New user' => 'Nuevo usuario',
    'Back' => 'Regresar',
    'Place' => 'Edificios',
    'Edit User' => 'Editar Usuario',
    'SocialArea' => 'Areas sociales',
    'Name' => 'Nombre',
    'Users' => 'Usuarios',
    'Actions' => 'Acciones',
    'Creation Date' => 'Creation Date',
    'Delete' => 'Eliminar',
    'Edit' => 'Editar',
    'Save' => 'Guardar',
    'Email' => 'Correo',
    'Role' => 'Rol',
    'Reservation' => 'Reservación',
    'Poll' => 'Encuesta',
    'Billing' => 'Facturación',
    'Claim' => 'Reclamo',
    'Profile' => 'Perfil',
    'Change Password' => 'Cambio de clave',
    'Home' => 'Home',
    'User successfully created.' => 'Usuario creado exitosamente.',
    'User successfully updated.' => 'Usuario actualizado exitosamente.',
    'User successfully deleted.' => 'Usuario eliminado exitosamente.',
    'Update' => 'Actualizar',
    'Type Message ...' => 'Escribe Mensaje ...',
    'Change password' => 'Cambio de clave',
    'Send' => 'Enviar',

    /***Users Module***/

    /*****Places*****/
    'Description' => 'Descripción',
    'New place' => 'Nuevo edificio',
    'Phone' => 'Teléfono',
    'Places' => 'Edificios',
    'Address' => 'Dirección',
    'Places successfully created.' => 'Edificio creado satisfactoriamente.',
    'Places successfully updated.' => 'Edificio modificado satisfactoriamente.',
    'Places successfully deleted.' => 'Edificio eliminado satisfactoriamente.',
    'Apartment' => 'Apartamento/Casa',
    'Other Information' => 'Otra información',
    /*****Places*****/

    /*******Reservations*******/

    'Reservations' => 'Reservaciones',
    'New reservation' => 'Nueva reservacion',
    'Reservation successfully created.' => 'Reservación creada satisfactoriamente',
    'Reservation successfully updated.' => 'Reservación modificada satisfactoriamente',
    'Reservation successfully deleted.' => 'Reservación eliminada satisfactoriamente',
    'Start' => 'Hora Desde',
    'End' => 'Hora Hasta',
    'Social' => 'Area social',
    'Date' => 'Fecha',
    'User' => 'Usuario',
    'Status' => 'Estado',
    'Wait' => 'Esperando',
    'Ready' => 'Listo',
    "Reservation don't was successfully created." => "La fecha que tomo, esta tomada",
    "Reservation don't was successfully edited." => "La fecha que tomo, esta tomada",
    'Reservation made by: ' =>  'Reservacion hecha por: ',
    /*******Reservations*******/

    /*******Claims*******/
    'Claims' => 'Reclamos',
    'New claim' => 'Nuevo reclamo',
    'Claim successfully created.' => 'Reclamo creado satisfactoriamente.',
    'Claim successfully updated.' => 'Reclamo modificada satisfactoriamente.',
    'Claim successfully deleted.' => 'Reclamo eliminada satisfactoriamente.',
    'Kind' => 'Tipo',
    'Password' => 'Clave',
    'Again Password' => 'Clave de nuevo',
    'Change' => 'Cambiar',
    'User successfully update password' => 'Usuario actualizo clave',
    'Anonymous' => 'Anonimo',
    'No Anonymous' => 'No Anonimo',
    /*******Claims*******/

    /*******/
    'Monday' => 'Lunes',
    'Tuesday' => 'Martes',
    'Wednesday' => 'Miercoles',
    'Thursday' => 'Jueves',
    'Friday' => 'Viernes',
    'Saturday' => 'Sabado',
    'Sunday' => 'Domingo',

    'January' => 'Enero',
    'February' => 'Febrero',
    'March' => 'Marzo',
    'April' => 'Abril',
    'May' => 'Mayo',
    'June' => 'Junio',
    'July' => 'Julio',
    'August' => 'Agosto',
    'September' => 'Septiembre',
    'November' => 'Noviembre',
    'December' => 'Diciembre',
    'of'=>'de',
    'main_navigation' => 'Menu',
    'change_password' => 'Canbio de clave',
    'account_settings' => 'Configuración'
    /*******/



];

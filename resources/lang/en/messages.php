<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines English
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /***Users Module***/
    'Dashboard' => 'Dashboard',
    'Direct Chat' => 'Direct Chat',
    'New user' => 'New user',
    'Place' => 'Places',
    'Back' => 'Back',
    'Edit User' => 'Edit User',
    'SocialArea' => 'Social Areas',
    'Name' => 'Name',
    'Users' => 'Users',
    'Actions' => 'Actions',
    'Creation Date' => 'Creation Date',
    'Delete' => 'Delete',
    'Edit' => 'Edit',
    'Are you sure' => 'Are you sure you want to delete this user?',
    'Save' => 'Save',
    'Email' => 'Email',
    'Role' => 'Role',
    'Reservation' => 'Reservation',
    'Poll' => 'Poll',
    'Billing' => 'Billing',
    'Claim' => 'Claim',
    'Profile' => 'Profile',
    'Change Password' => 'Change password',
    'Home' => 'Home',
    'User successfully created.' => 'User successfully created.',
    'Update' => 'Update',
    'User successfully updated.' => 'User successfully updated.',
    'User successfully deleted.' => 'User successfully deleted.',
    'Type Message ...' => 'Type Message ...',
    'Send' => 'Send',
    'Change password' => 'Change password',
    /***Users Module***/

    /*****Places*****/
    'Description' => 'Descriptions',
    'Phone' => 'Phone',
    'New place' => 'New place',
    'Places' => 'Places',
    'Address' => 'Address',
    'Places successfully created.' => 'Places successfully created.',
    'Places successfully updated.' => 'Places successfully updated.',
    'Places successfully deleted.' => 'Places successfully deleted.',
    'Apartment' => 'Apartment/House',
    'Other Information' => 'Other information',
    /*****Places*****/

    /*******Reservations*******/
    'Reservations' => 'Reservations',
    'New reservation' => 'New reservation',
    'Reservation successfully created.' => 'Reservation successfully created.',
    'Reservation successfully updated.' => 'Reservation successfully updated.',
    'Reservation successfully deleted.' => 'Reservation successfully deleted.',
    'Start' => 'Start',
    'End' => 'End',
    'Social' => 'Social Area',
    'Date' => 'Date',
    'User' => 'User',
    'Status' => 'Status',
    'Wait' => 'Wait',
    'Ready' => 'Ready',
    "Reservation don't was successfully created." => "Reservation don't was successfully created",
    "Reservation don't was successfully edited." => "Reservation don't was successfully edited.",
    'Reservation made by: ' =>  'Reservation made by: ',
    /*******Reservations*******/

    /*******Claims*******/
    'Claims' => 'Claims',
    'New claim' => 'New claim',
    'Claim successfully created.' => 'Claim successfully created.',
    'Claim successfully updated.' => 'Claim successfully updated.',
    'Claim successfully deleted.' => 'Claim successfully deleted.',
    'Kind' => 'Kind',
    'Password' => 'Password',
    'Again Password' => 'Again Password',
    'Change' => 'Change',
    'User successfully update password' => 'User successfully update password',
    'Anonymous' => 'Anonymous',
    'No Anonymous' => 'No Anonymous',
    /*******Claims*******/

    /*******/
    'Monday' => 'Monday',
    'Tuesday' => 'Tuesday',
    'Wednesday' => 'Wednesday',
    'Thursday' => 'Thursday',
    'Friday' => 'Friday',
    'Saturday' => 'Saturday',
    'Sunday' => 'Sunday',

    'January' => 'January',
    'February' => 'February',
    'March' => 'March',
    'April' => 'April',
    'May' => 'May',
    'June' => 'June',
    'July' => 'July',
    'August' => 'August',
    'September' => 'September',
    'November' => 'November',
    'December' => 'December',
    'of'=>'of',
    'MAIN NAVIGATION' => 'Main Navigation',
    'change_password' => 'Change password',
    'ACCOUNT SETTINGS' => 'Account settings'
    /*******/




];

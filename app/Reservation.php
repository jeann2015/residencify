<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'start', 'end', 'user_id','social_id','status'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}

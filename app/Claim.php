<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    protected $fillable = [
        'details', 'kind', 'user_id','status'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}

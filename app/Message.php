<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'message', 'user_id', 'place_id','created_at','updated_at'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function place(){
        return $this->belongsTo('App\Place');
    }
}

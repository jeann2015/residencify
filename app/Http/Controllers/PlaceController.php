<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlaceRequest;
use App\Place;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PlaceController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Factory|View
     */
    public function index(){
        $places = Place::all();
        return view('places.index', compact('places','users'));
    }

    /**
     * @return Factory|View
     */
    public function create(){
        $users = User::where('role','Chief')->get();
        return view('places.create', compact('users'));
    }

    /**
     * @param PlaceRequest $request
     * @return mixed
     */
    public function store(PlaceRequest $request) {

        Place::create([
            'description' => $request['description'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            'chief_users_id' => $request['chief_users_id'],
        ]);
        return redirect()->route('places')->withStatus(__('messages.Places successfully created.'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($id){
        $places = Place::find($id);
        $users = User::where('role','Chief')->get();
        return view('places.edit', compact('users','places'));
    }

    /**
     * @param PlaceRequest $request
     * @return mixed
     */
    public function update(PlaceRequest $request) {
        Place::where('id',$request['id'])
            ->update([
            'description' => $request['description'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            'chief_users_id' => $request['chief_users_id'],
        ]);
        return redirect()->route('places')->withStatus(__('messages.Places successfully updated.'));
    }

    public function delete($id){
        $places = Place::find($id);
        $users = User::where('role','Chief')->get();
        return view('places.delete', compact('users','places'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request) {
        Place::where('id',$request['id'])->delete();
        return redirect()->route('places')->withStatus(__('messages.Places successfully deleted.'));
    }

}

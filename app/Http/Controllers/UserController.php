<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Place;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Factory|View
     */
    public function index(){
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * @return Factory|View
     */
    public function create(){
        $places = Place::all();
        return view('users.create', compact('places'));
    }

    /**
     * @param UserRequest $request
     * @return mixed
     */
    public function store(UserRequest $request) {
        $chief_id = User::where('role','Chief')->get();
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make('123123123'),
            'role' => $request['role'],
            'first_time' => 1,
            'chief_users_id' => $chief_id->pluck('id')[0],
            'place_id' => $chief_id->pluck('place_id')[0],
            'apartment' => $request['apartment'],
            'description' => $request['description'],
        ]);
        return redirect()->route('users')->withStatus(__('User successfully created.'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($id){
        $users = User::find($id);
        $places = Place::all();
        return view('users.edit', compact('users','places'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request) {
        $chief_id = User::where('role','Chief')->get();
        User::where('id',$request['id'])->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'role' => $request['role'],
            'chief_users_id' => $chief_id->pluck('id')[0],
            'place_id' => $chief_id->pluck('place_id')[0],
            'apartment' => $request['apartment'],
            'description' => $request['description'],
        ]);

        return redirect()->route('users')->withStatus(__('User successfully updated.'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function delete($id){
        $users = User::find($id);
        $places = Place::all();
        return view('users.delete', compact('users','places'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request) {
        User::where('id',$request['id'])->delete();
        return redirect()->route('users')->withStatus(__('User successfully deleted.'));
    }

    /**
     * @return Factory|View
     */
    public function password(){
        return view('users.password');
    }


    /**
     * @param Request $request
     * @return Factory|View
     */
    public function change(Request $request){
        User::where('id',$request['id'])->update([
           'password' => Hash::make($request['password'])
        ]);
        return redirect()->route('admin.password')->withStatus(__('User successfully update password.'));
    }

}

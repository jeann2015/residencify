<?php

namespace App\Http\Controllers;
use App\Message;
use App\Reservation;
use App\Social;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ReservationController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Factory|View
     */
    public function index(){
        if(auth()->user()->role == 'Chief') {
            $reservations = Reservation::all();
        } else {
            $reservations = Reservation::where('user_id', Auth::id())->orderBydesc('id')->get();
        }
        return view('reservations.index', compact('reservations'));
    }

    /**
     * @return Factory|View
     */
    public function create(){
        $socials = Social::all();
        $users = User::where('role','<>','Admin')->get();
        return view('reservations.create',compact('socials','users'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request) {

        $startCreate = date_create($request['dateReservation']." ".$request['start']);
        $start = date_format($startCreate,"Y-m-d H:i:s");
        $endCreate = date_create($request['dateReservation']." ".$request['end']);
        $end = date_format($endCreate,"Y-m-d H:i:s");

        $existReservations = Reservation::where('start',$start)->where('end',$end)->get();

        if($existReservations->count() == 0) {
            Reservation::create([
                'start' => $start,
                'end' => $end,
                'social_id' => $request['social_id'],
                'user_id' => Auth::id(),
                'status' => 'Wait'
            ]);

            return redirect()->route('reservations')->withStatus(__('messages.Reservation successfully created.'));
        } else {
            return back()->withStatus(__("messages.Reservation don't was successfully created."));
        }
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($id){
        $reservations = Reservation::find($id);
        $users = User::where('role','<>','Admin')->get();
        $socials = Social::all();
        return view('reservations.edit', compact('socials','users','places','reservations'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function view($id){
        $reservations = Reservation::find($id);
        $users = User::where('role','<>','Admin')->get();
        $socials = Social::all();
        return view('reservations.view', compact('socials','users','places','reservations'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request) {
        if(auth()->user()->role == 'Chief') {
            Reservation::where('id',$request['id'])->update([
                'status' => $request['status']
            ]);
            if($request['status'] == 'Ready'){
                $r = Reservation::find($request['id']);
                Message::create([
                    'message' => "Reservacion hecha por:".$r->user->name." (".$r->user->apartment.") ".$r->start .", hasta: ".$r->end,
                    'user_id' => auth()->user()->id,
                    'place_id' => auth()->user()->place_id
                ]);
            }
        } else {
            $startCreate = date_create($request['dateReservation']." ".$request['start']);
            $start = date_format($startCreate,"Y-m-d H:i:s");
            $endCreate = date_create($request['dateReservation']." ".$request['end']);
            $end = date_format($endCreate,"Y-m-d H:i:s");

            $existReservations = Reservation::where('start',$start)->where('end',$end)->get();

            if($existReservations->count() == 0) {
                Reservation::where('id', $request['id'])->update([
                    'start' => $start,
                    'end' => $end,
                    'social_id' => $request['social_id'],
                    'status' => $request['status'],
                ]);
            }
        }
        return redirect()->route('reservations')->withStatus(__('messages.Reservation successfully updated.'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function delete($id){
        $reservations = Reservation::find($id);
        $users = User::where('role','<>','Admin')->get();
        $socials = Social::all();
        return view('reservations.delete', compact('socials','users','places','reservations'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request) {
        Reservation::where('id',$request['id'])->delete();
        return redirect()->route('reservations')->withStatus(__('messages.Reservation successfully deleted.'));
    }

}

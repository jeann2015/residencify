<?php

namespace App\Http\Controllers;

use App\Claim;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ClaimController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Factory|View
     */
    public function index(){
        if(auth()->user()->role == 'Chief') {
            $claims = Claim::all();
        } else {
            $claims = Claim::where('user_id', Auth::id())->orderBydesc('id')->get();
        }
        return view('claims.index', compact('claims'));
    }

    public function create(){
        return view('claims.create');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request) {
        Claim::create([
            'details' => $request['details'],
            'kind' => $request['kind'],
            'status' => 'Wait',
            'user_id' => Auth::id(),
        ]);
        return redirect()->route('claims')->withStatus(__('messages.Claim successfully created.'));
    }

    /**
     * @param $id
     * @return Factory|View
     */
    public function edit($id){
        $claims = Claim::find($id);
        return view('claims.edit', compact('claims'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request) {
        if(auth()->user()->role == 'Chief') {
            Claim::where('id',$request['id'])->update([
                'status' => $request['status']
            ]);
        } else {
            Claim::where('id', $request['id'])->update([
                'details' => $request['details'],
                'kind' => $request['kind'],
                'status' => 'Wait',
            ]);
        }
        return redirect()->route('claims')->withStatus(__('messages.Claim successfully updated.'));
    }
}

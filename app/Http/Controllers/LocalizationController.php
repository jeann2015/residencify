<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LocalizationController extends Controller
{
    public function index(Request $request)
    {
        App::setLocale($request['locale']);
        session()->put('locale', $request['locale']);
        return redirect()->back();
    }
}

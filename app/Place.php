<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $fillable = [
        'description', 'phone', 'address','chief_users_id'
    ];


    public function message(){
        return $this->hasMany('App\Message');
    }
}
